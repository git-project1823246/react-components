// src/components/CourseCard.js
import React from 'react';
import styled from 'styled-components';
import Button from '../Button/Button';

const CardContainer = styled.div`
  display: flex;
  justify-content: space-between;
  border: 1px solid #e9ecef;
  border-left: 5px solid black; /* Линия слева */
  border-radius: 4px;
  padding: 20px;
  margin: 20px 100px;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
`;

const LeftContent = styled.div`
  flex: 1;
  padding-right: 40px; /* Добавлен паддинг справа */
`;

const RightContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
`;

const Title = styled.h2`
  margin-top: 0;
  margin-bottom: 10px;
  font-size: 24px;
  font-weight: bold;
`;

const Description = styled.p`
  color: black;
`;

const InfoText = styled.p`
  margin: 5px 0;
  font-weight: bold;
`;

const ButtonContainer = styled.div`
  margin-top: 20px;
  align-self: flex-start;
`;

const CourseCard = ({ course, authors }) => {
  const { title, description, creationDate, duration, authors: courseAuthors } = course;

  const formatDuration = (minutes) => {
    const hours = Math.floor(minutes / 60);
    const mins = minutes % 60;
    return `${hours < 10 ? '0' : ''}${hours}:${mins < 10 ? '0' : ''}${mins} ${hours === 1 ? 'hour' : 'hours'}`;
  };

  const getAuthorsNames = (authorIds) => {
    return authorIds
      .map((id) => {
        const author = authors.find((author) => author.id === id);
        return author ? author.name : '';
      })
      .join(', ');
  };

  return (
    <CardContainer>
      <LeftContent>
        <Title>{title}</Title>
        <Description>{description}</Description>
      </LeftContent>
      <RightContent>
        <InfoText>Authors: {getAuthorsNames(courseAuthors)}</InfoText>
        <InfoText>Duration: {formatDuration(duration)}</InfoText>
        <InfoText>Created: {creationDate}</InfoText>
        <ButtonContainer>
          <Button buttonText="Show course" onClick={() => {}} />
        </ButtonContainer>
      </RightContent>
    </CardContainer>
  );
};

export default CourseCard;
